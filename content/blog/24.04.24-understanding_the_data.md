title: Understanding the data
date: 2024 04 24

Today we looked at the first results of the data visualisation graphs that Gijs made using the Javascript library [d3](https://www.fullstackpython.com/d3-js.html). d3 or "Data-Driven Documents (d3.js) is a JavaScript visualization library used to create interactive visuals for web browsers".

Some interesting facts appeared.

![]({static}/images/blog/graph5.png)

**NOTE GIJS**: can you take out the filtered CO2 at Douglasfir? As we don't know what exactly it does?
This also would avoid bad interpretations of big peaks of CO2 with Douglas fir in the morning (is it rush hour? check the timezone!)

The Solitary Oak registers a higher CO2-level than the other trees.
We start asking ourselves questions.
In general, all CO2, air temperature, air humidity and air pressure of Solitary Oak are always a little bit higher than the others. Is it possible that we didn't calibrate the sensor in the same way? We don't know...
Is it possible that Solitary Oak is surrounded by more CO2 and more other elements because it is standing alone and closer to a busy road?
The Horse Chestnut registers considerably less CO2 than the other trees. But then, the Horse Chestnut is being equipped by a Sensecap sensor while four others are equipped with the same Milesight sensor.
Is it possible that registration differs from company to company, that the Sensecap sensor is formatted with a different baseline? It shouldn't be. And the proof is that the Douglas fir, equipped with a Decentlab CO2-sensor, shows the same regular curves as the others.
How do we deal with these questions? Do we leave them open to the visitors?


**NOTE GIJS**: can you please create the graph of soil humidity at all trees?

The Solitary Oak also registers much more soil humidity.
This makes sense as the Solitary Oak is living in the very swampy area of the Moerasweide, where all water coming down from Hoge Veluwe is gathered.

![]({static}/images/blog/graph4.png)

Gijs' work makes beautifully clear how air temperature and relative humidity at the branches are inversely related.
When the air temperature drops, the humidity increases.
It makes sense to any visitor of the park who wanders around at night after a relatively warm day: temperature drops and the grass becomes wet.

Sunshine and sapflow are in regular rhythms at the Douglas Fir. The more sun there is, the faster the sapflow moves.
Of course, tree experts would say. It is photosynthesis. We're happy to also see it on our screens.

**NOTE GIJS**: in the graph of the Douglas fir, could you please take out soil temperature, air pressure, sensoterra index? (https://bomenvertellen.net/multigraph/sensor/11,12,20,26,27,28,37,46,54/7)

![]({static}/images/blog/graph1.png)

We also noticed that the Sensoterra sensor at Red Beech is showing peaks in soil temperature that make us wonder. The first peak is on Thursday 18-4 at 8:39h, where it jumps from 5°C (???) tot 28°C. The second peak is on Tuesday 23-4 at 8:39h again, where it peaks to 35°C. Can sensors have disobedient behaviour? Is it just a bug?
Where exactly are the Sensoterra sensors measuring the soil temperature? It is at the surface, where remaining water can warm up quickly in the sun, or is it at the far end of the sensor, 30cm in the ground?
Apart of that, the temperature sensor of the Douglas fir is not sending any data.
We're contacting the factory to see what happens.
