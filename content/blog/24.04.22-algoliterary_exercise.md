title: Algoliterary exercise
date: 2024 04 22

Once the database was in place, it invited to make 'the data speak' in some sorts.

After creating a few functions in Python, it became easy to generate small sentences, like:

Mijn wortels ervaarden vandaag het meeste vocht om 22:27 uur. Het ging om 6.10 %.
My roots experienced most moisture today at 22:27 h, it was 6.10 %.

Op dit moment ervaren mijn wortels een vochtigheidsgraad van 6.00 %.
Dat is 0.00 % meer dan 15 minuten geleden.
At this moment my roots experience a degree of moisture of 6.00 %.
That's 0.00 % meer than 15 minutes ago.

Gisteren op hetzelfde uur ervaarden mijn wortels een vochtigheidsgraad van 6.20 %.
Op dit moment ervaren ze 0.10 % minder dan op hetzelfde moment gisteren.
Yesterday at the same time my roots experiences a degree of moisture of 6.20 %.
At this moment they experience 0.10 % minder than at the same moment yesterday.

Van alle bomen die deelnemen aan dit project, ervaarde Allenige Eik de grootste hoeveelheid bodemvochtigheid vandaag: 35.9 % om 05:37.
Of all of us participating, Solitary Oak experienced the highest degree of soil moisture today: 35.9 % at 05:37.

Rode Beuk ervaarde de tweede grootste hoeveelheid bodemvochtigheid vandaag: 15.40 % om 22:39.
Allenige Eik ervaarde 20.50 % meer dan Rode Beuk tijdens de hoogste meting van bodemvochtigheid.
Red Beech experienced the second highest amount of soil moisture today: 15.40 % at 22:39.
Solitary Oak experienced 20.50 %% meer than Red Beech at the highest measurement of soil moisture.

Van alle bomen die deelnemen aan dit project, ervaarde Kastanje de minste hoeveelheid bodemvocht vandaag: 6.1 % om 21:27.
Of all of us participating, Sweet Chestnut experienced the lowest level of soil moisture today: 6.1 % at 21:27.

Rode Beuk ervaarde de tweede laagste hoeveelheid bodemvocht vandaag: 15.00 % om 21:39.
Kastanje ervaarde 8.90 % minder dan Rode Beuk op de laagste bodemvochtigheidsmeting.
Red Beech experienced the second lowest amount of soil moisture today: 15.00 % at 21:39.
Sweet Chestnut experienced 8.90 % minder than Red Beech at the lowest measurement of soil moisture.

Mijn wortels ervaren 0.10 % bodemvocht minder dan op het hoogst gemeten moment vandaag, om 22:27 uur.
My roots experience 0.10 % soil moisture less than at the highest measurement today, at 22:27 uur.

Git repository: [https://gitlab.constantvzw.org/anais_berck/gesitueerd-portret](https://gitlab.constantvzw.org/anais_berck/gesitueerd-portret)
