Title: About
Lang: en
Summary: short on the project

About the project

'Bomen Vertellen' is an artistic research and installation project that runs from March 24 till June 25. It will first be installed and produced in the parks of Arnhem, celebrating the 125th anniversary of the parks of Sonsbeek, Zijpendaal and Gulden Bodem.
The installation 'Arnhemse Bomen Vertellen' will run from 15 June till 15 September 2024. The audience will be able to walk along the parks visiting six remarkable trees. Each tree carries an original QR-code that activates a webpage with a portrait of the tree. The images and sounds of the portrait update every hour following the dataflows of the life of the tree that usually remains invisible to the human eye: humidity and temperature of the soil and air, CO2, sapflow, sunlight, pressure, wind...
From the Summer onwards the project will have more experimental phases in four Flemish nature reserves: Sonian Forest, De Liereman, Park Hoge Kempen and Kalmthoutse Heide.

This project is realized by Anaïs Berck, a collective of humans, trees and algorithms:

- artists collaborating in this project: Gijs de Heij, Doriane Timmermans, An Mertens, Christina Clar, Bram Goots
- trees collaborating in this project: Poortwachter Kastanje, Rode Beuk Zijpendaal, Paardenkastanje Witte Villa, Solitaire Eik Sonsbeek, Eik Ronde Weide Sonsbeek, Douglasspar (Arnhem), remarkable Oak Friends of Sonian, remarkable Beech Friends of Sonian (Brussels)
- algorithms collaborating in this project: see the documentation of sensors & image/sound processing

'Arnhemse Bomen Vertellen' would not exist without the significant support of the Commune of Arnhem:

- materialisation: Marcel Wenker, Rob Horst
- financing: Marko Vanderwel, Wethouder Natuur, Milieu, Zorg, Lokaal Voedsel, Water
- installation sensors: Ruud Krabbenborg, Arthur Wanders, Tony Berendsen, Lucas Willen Beemster, Dave Bobeldijk, Tim van Leeuwen
- communication: Sanne Blok, Hanneke Busscher
- production: Karlijn Michielsen
- furniture: Floris van Hintum

Thanks to Dr. Ute Sass-Klaassen, ir. Erik Mertens, Joost Verhagen (for their advice); parkgids Eddy van Heeckeren, bosexpert Simon Klingen, ecoloog Hans Van den Bos, artist Boudewijn Corstiaensen, managers Jan Floor, Rob Borst and Marcel Wenker, nature guide en biologist Pim van Oers, park guide Gerard Herbers, artist Albert van der Weide, forest guard Nina Kreisler, author Jibbe Willems, city ecologist Ineke Wesseling (for sharing their knowledge and experiences)

'Situated Portrait' which is the name of the project in Flanders would not exist without the support of:

- financing: Vlaamse Overheid, Kunsten & Erfgoed
- partners: Brussels Leefmilieu, GC Wabo, Natuurgebied De Liereman, CC De Warande, Park Hoge Kempen, Kunstencentrum Jester, Van Dorenmuseum, De Vroente
