title: Glossary

![]({static}/images/network_of_tree.jpg)

<details>
    <summary>Iot</summary>

IoT data logging is a tool for researchers, farmers, golf and other managers and scientists involved in soil, plant, or environmental monitoring. By logging measurements as frequently as 15-minute intervals, over 35,000 measurements can be made per year. This volume of data enables the development of big data models for detailed analysis of multiple parameters.

</details>

<details>
    <summary>LoraWAN</summary>

LoRaWAN (Long Range Wide Area Network) is a wireless protocol that operates in the 800-900MHz band and provides access for IoT devices using a Gateway connected to the internet or local server. With range measurements of up to 100km recorded, the range is dependent on the local physical environment, such as buildings and other hard infrastructure that can restrict signal transmission. This can restrict urban communication to 2-5km range, whilst the landscape can reduce rural transmission to 15km or less.

From [Wikipedia](https://en.wikipedia.org/wiki/LoRa):
LoRa (from "long range") is a physical proprietary radio communication technique.[1] It is based on spread spectrum modulation techniques derived from chirp spread spectrum (CSS) technology.[2] It was developed by Cycleo, a company of Grenoble, France, and patented in 2014.[3] Cycleo was later acquired by Semtech.[4]

LoRaWAN (wide area network) defines the communication protocol and system architecture. LoRaWAN is an official standard of the International Telecommunication Union (ITU), ITU-T Y.4480.[5] The continued development of the LoRaWAN protocol is managed by the open, non-profit LoRa Alliance, of which SemTech is a founding member.

Together, LoRa and LoRaWAN define a low-power, wide-area (LPWA) networking protocol designed to wirelessly connect battery operated devices to the internet in regional, national or global networks, and targets key Internet of things (IoT) requirements such as bi-directional communication, end-to-end security, mobility and localization services. The low power, low bit rate, and IoT use distinguish this type of network from a wireless WAN that is designed to connect users or businesses, and carry more data, using more power. The LoRaWAN data rate ranges from 0.3 kbit/s to 50 kbit/s per channel.
The LoRa Alliance is an open, non-profit association whose stated mission is to support and promote the global adoption of the LoRaWAN standard for massively scaled IoT deployments, as well as deployments in remote or hard-to-reach locations.

Members collaborate in a vibrant ecosystem of device makers, solution providers, system integrators and network operators, delivering interoperability needed to scale IoT across the globe, using  public, private, hybrid, and community networks. Key areas of focus within the Alliance are Smart Agriculture, Smart Buildings, Smart Cities, Smart Industry, Smart Logistics, and Smart Utilities.

Key contributing members of the LoRa Alliance include Actility, Amazon Web Services, Cisco. Everynet, Helium, Kerlink, MachineQ, a Comcast Company, Microsoft, MikroTik, Minol Zenner, Netze BW, Semtech, Senet, STMicroelectronics, TEKTELIC, and The Things Industries. In 2018, the LoRa Alliance had over 100 LoRaWAN network operators in over 100 countries; in 2023, there are nearly 200, providing coverage in nearly every country in the world.
</details>

<details>
    <summary>SigFox</summary>
</details>

<details>
    <summary>Increment borer</summary>
</details>

<details>
    <summary>Sapstream</summary>
</details>

<details>
    <summary>CO2</summary>
</details>

<details>
    <summary>AGPS</summary>
</details>

<details>
    <summary>Dripping line</summary>
</details>

<details>
    <summary>NFC<summary>
</details>

<details>
    <summary>Payload<summary>
    written in js, computer science term
</details>
