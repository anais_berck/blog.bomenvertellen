title: Solar radiation

## Device

![]({static}/images/devices/decentlab.jpg)

Following the Decentlab website, the solar radiation device is used for solar panel arrays, smart agriculture, and building automation.

The study and measurement of solar irradiance have several important applications, including the prediction of energy generation from solar power plants, the heating and cooling loads of buildings, climate modelling and weather forecasting, passive daytime radiative cooling applications, and space travel. (Wikipedia)

## How does it work?

The solar radiation device is a small ball, the size of an eye, that has to be mounted horizontally in an open space. it measures the solar irradiance, the power per unit area (surface power density) received from the Sun in the form of electromagnetic radiation in the wavelength range of the measuring instrument. The SI unit of irradiance is joules per second per square metre (W/m−²). The unit of insolation often used in the solar power industry is kilowatt hours per square metre (kWh/m²).
Joule expresses the amount of energy, while Watt indicates the power - if you apply the power over an amount of time (f.ex. per hour), then you have consumed/generated a certain amount of Joule. The sensor uses as a unit joules per second per square metre (W/m−2). Divided by 1000 it gives kWh/m²).

Because the Earth is round, the sun strikes the surface at different angles, ranging from 0° (just above the horizon) to 90° (directly overhead). When the sun's rays are vertical, the Earth's surface gets all the energy possible. The more slanted the sun's rays are, the longer they travel through the atmosphere, becoming more scattered and diffuse. Because the Earth is round, the frigid polar regions never get a high sun, and because of the tilted axis of rotation, these areas receive no sun at all during part of the year. [Source](https://www.energy.gov/eere/solar/solar-radiation-basics)

Sunshine duration is currently defined as the period during which direct solar radiation exceeds the fixed limit of 120 W/m². In the traditional way in the past, the sunshine duration was measured using a heliograph. This consists of a glass sphere that focuses the direct solar radiation on heat-sensitive paper. This leaves a burn track and from length of this one can determine the sunshine duration.[](https://www.meteo.be/nl/klimaat/klimaat-van-belgie/klimaatatlas/klimaatkaarten/zonnestraling/meer-info).

The average monthly sunshine radiation for 1991-2020 for the Netherlands (De Bilt) can be found here: [](https://www.knmi.nl/klimaatdashboard)


## Manufacturer

[Decentlab](https://www.decentlab.com/) is a Swiss company "providing wireless device devices and services for distributed, cost-effective monitoring solutions. The device devices communicate wirelessly over LoRaWAN® and are designed for ultra low power consumption, capable of operating on batteries for several years.
It is a spin off from the Swiss Federal Laboratories for Materials Science and Technology (Empa). The company was founded in 2008 by the combined expertise of Jonas Meyer and Reinhard Bischoff in electrical engineering and system design. Together they started with structural health monitoring of infrastructures in 2005 across Europe with the objective of reducing the installation and maintenance costs. Since then the company has expanded into other areas such as environmental monitoring and smart-city projects.
Our monitoring products are deployed world-wide over more than 40 countries."
The original device was bought by them from Apogee.

## Setup

We installed the solar radiation device on a lamp post at the nearby manège.
As the lamp post is standing next to the parking lot, providing light onto the horse circuit, it stands free from any obstruction. The sun reaches the device 12h/day.

## Data

devices 2 api 2 app pipeline, what did they provide?

what does the data look like?

## Why measuring solar radiation?

Photosynthesis is a system of biological processes by which photosynthetic organisms, such as most plants, algae, and cyanobacteria, convert light energy, typically from sunlight, into the chemical energy necessary to fuel their activities. Photosynthetic organisms use intracellular organic compounds to store the chemical energy they produce in photosynthesis within organic compounds like sugars, glycogen, cellulose and starches. SOurce: [Wikipedia](https://en.wikipedia.org/wiki/Photosynthesis)

### Plants have more evolved cells
Both animal and plant cells are eukaryotes, meaning they have a cell nucleus that contains chromosomes. Both have cell membranes surrounding the cell that regulate the movement of substances in and out of the cell. Differences in these two types of cells arise from functional differences.

One of the biggest differences between a plant and an animal cell is the presence of a cell wall composed of cellulose in plants. This allows plants to build up high pressure inside the cell without bursting. This cell wall is necessary in the case of plants, as plant cells require heavy exchange of fluids due to osmosis. Animal cells do not have this cell wall.

Another difference arises from the use of photosynthesis, a process by which plants convert sunlight into food. For this purpose, plants have chloroplasts with their own DNA. This is absent in animal cells.

Plant cells have a large vacuole present in the cytoplasm of cells. This vacuole occupies all the space in a plant cell with a cell membrane surrounding it. This vacuole contains waste products, water and nutrients that plants can use or secrete when needed. On the other hand, animal cells have small vacuoles compared to plant cells that have a large vacuole. Another notable difference is that plant cells are usually regular-sized, while animal cells vary greatly in size and shape. In general, plant cells are larger in size than animal cells. In terms of shape, plant cells are rectangular in shape, while animal cells are circular.
Source: [https://nl.differkinome.com/articles/biology-science-nature/difference-between-animal-cell-and-plant-cell.html](https://nl.differkinome.com/articles/biology-science-nature/difference-between-animal-cell-and-plant-cell.html)
