title: Sapflow device


## Measurements

![]({static}/images/devices/decentlab.jpg)

The Implexx Sap Flow device is used to measure the sapflow or stem water of a tree or plant.
Stem water content is a measure of the hydration levels in stem tissues. Hydration, or the amount of water, is critical for a large range of plant physiological processes including transpiration, photosynthesis, reproduction, growth, organ development, cell structure and function, osmotic relations, and more.
Stem water content is also important for several parameters of interest to hydrologists such as storage (capacitance) and the abilities of plants to respond to soil moisture deficits, droughts and extreme weather events.

Sapflow devices are used by scientific researchers, agronomers and farmers.
Studies on physiological drought can profit from simultaneous measurements of sap flow and stem diameter ,variations on individual trees in order to assess global tree physiological responses to drought, as these measurements are relatively easy to obtain and reflect in an integrated way the hydraulic and carbon status of the tree.
The rate of sap (or water) moving from the roots of plants, through the stem, and to the leaves is strongly coupled to rates of transpiration and photosynthesis (Morén et al. 2001). Consequently, sap flow measurements can be used to constrain estimates of tree-level and stand-level water and carbon fluxes in ecosystems.
Trees play an important role in both the water and carbon cycles. devices that measure the flow rate of sap in individual trees provide critical insight into water use dynamics, and these data are vital for the understanding of ecosystem functions.
Quantifying sap flow is key to understanding the mechanistic drivers of fluxes of water and carbon in trees, helping to inform landscape management decisions (Pataki et al. 2000) and predict ecosystem responses to global change (Raczka et al. 2018). Large networks of sap flow devices have the potential to dramatically improve our ability to model ecosystem function and scale observations from individual trees to a forest stand, region, and ultimately the globe (Kume et al. 2010). Even though there are challenges when utilizing sap flow data in this way (Ameye et al. 2012, Flo et al. 2019), it is increasingly being recognized that the diversity of water use strategies by different plant species has global-scale implications, especially in the context of climate change (Keenan et al. 2013, Anderegg et al. 2018).  (<https://esajournals.onlinelibrary.wiley.com/doi/full/10.1002/ecs2.3135>)

## Manufacturer

[Decentlab](https://www.decentlab.com/) is a Swiss company "providing wireless device devices and services for distributed, cost-effective monitoring solutions. The device devices communicate wirelessly over LoRaWAN® and are designed for ultra low power consumption, capable of operating on batteries for several years.
It is a spin off from the Swiss Federal Laboratories for Materials Science and Technology (Empa). The company was founded in 2008 by the combined expertise of Jonas Meyer and Reinhard Bischoff in electrical engineering and system design. Together they started with structural health monitoring of infrastructures in 2005 across Europe with the objective of reducing the installation and maintenance costs. Since then the company has expanded into other areas such as environmental monitoring and smart-city projects.
Our monitoring products are deployed world-wide over more than 40 countries."

## how does it work?

Stem water content is measured destructively or estimated with device technology.
Destructive measurements are the most accurate method to determine stem water content. But, as
the name implies, it also means wounding, damaging, or harvesting the plant.
In trees, a sample is collected via a stem borer. The wet weight of the sample is quickly measured on
a weigh scale. The sample is then oven dried to remove all moisture and the sample is reweighed to
find the dry weight. The water content of the sample can then be calculated.
device technology is an in-situ, continuous and non-destructive estimate of stem water content.
Sensing technology is not a direct measurements of water content rather it is an estimate based on
physical principles. This may include capacitance devices measuring the dielectric permittivity of
stems; or thermal devices measuring the thermal properties of stems via thermal conduction. The
dielectric permittivity or thermal properties of the stem are then converted to water content
through a series of equations.
The Implexx Sap Flow device (Figure 8) estimates stem water content via thermal properties and
thermal conduction.

The sap flow device is an instrumentation technique in which devices are introduced in the plant xylem at stem or trunk to measure temperature differences that indicates sap flow information, such as direction and density (Acosta-Navarrete etal.,2014). Sap flow rate in the xylem of plant stems is estimated by measuring the velocity of a pulse of heat introduced into the stem. The heat pulse is introduced with a line heat source, inserted radially into the stem or trunk. A temperature probe, positioned downstream and upstream from the line heat source, is used to measure the travel time of the heat pulse at various depths within the xylem. Thermocouples or thermistors are positioned along the length of the temperature probes to monitor temperature rise at the inner and outer positions within the stem. At each position, the heat pulse velocity is determined by measuring the time between the introduction of the heat pulse and the occurrence of the maximum temperature rise. Heat pulse velocities are converted to sap flux densities and then integrated over the cross-sectional area of the stem or trunk to yield the volumetric sap flow rate - using equations.

The Implexx Sap Flow device can estimate stem water content at multiple depths into the stem.
Known as “radial profiling”, measurements are undertaken towards the outside (“outer”) and inside
(“inner”) portions of the stem. The 30 mm length needles of the Implexx Sap Flow device
have a temperature device (thermistor) at 10 mm (outer) and 20 mm (inner) depth. Therefore, these
two measurement locations can be used to resolve stem water content at different locations within
the stem’s anatomy.

The observable range of values for stem water content is between 0 % (completely dry) and 100 %
(pure water). Typically, values range between 10 % and 80 % as some moisture is always retained in
the stem and the stem cannot be pure water.

When installing sap flow device needles, flow obstruction in the conductive tissue occurs. This wound effect is known to cause underestimations for heat pulse methods (Burgess et al. 2001, Green et al. 2009, Vandegehuchte and Steppe 2012), but it is also likely to affect other needle-based methods. While wound correction factors have so far only been based on wound width, the underestimation will also be dependent on the thermal properties of the sapwood.(<https://watermark.silverchair.com/tpv033.pdf>)

Some references

- Barrett DJ, Hatton TJ, Ash JE, Ball MC (1995) Evaluation of the heat pulse velocity technique for
measurement of sap flow in rainforest and eucalypt forest species of South-Eastern
Australia. Plant Cell Environ 18:463–469.
- Cohen Y, Fuchs M, Green GC (1981) Improvement of the heat pulse method for determining sap flow
in trees. Plant Cell Environ 4:391–397.
- Forster MA (2014) How significant is nocturnal sap flow? Tree Physiol. 34:757–765.

## Setup

We installed the sapflow device at the Douglas fir in Schaarsbergen-Zuid.
First we drilled 3 little holes in the stem to fit the sleeves and the needles of the device.
Next, we installed the sender/receiver, together with a solar panel and battery pack.
After a few weeks we had to replace the solar panel by a lead battery for more steady performance.

## Data

The data is sent every hour from the device to the server of Decentlab. We retrieve the data using an api (in Pandas).

The following elements are measured:

- sapflow alpha inner
- sapflow alpha outer
- sapflow beta inner
- sapflow beta outer
- sapflow heat velocity inner
- sapflow heat velocity outer
- sapflow sap-flow
- sapflow temperature outer
- sapflow tmax inner
- sapflow tmax inner upstream
- sapflow tmax outer
- sapflow tmax outer upstream
- sapflow voltage max
- sapflow voltage min
