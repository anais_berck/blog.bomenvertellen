title: Soil moisture and temperature


## Device

![]({static}/images/devices/sensoterra.jpg)

This device measures at the [drip line of the tree](https://www.hunker.com/12499623/how-to-figure-the-drip-line-of-a-tree), near its roots:

* **soil temperature** in °C
* **soil moisture** in %
* **Sensoterra Index**: The Sensoterra Index simplifies soil moisture percentage values into "Too Wet," "Too Dry," and "Healthy" regions. This simplified view allows for quick decision making and combining sensors with different soil types in an easy to understand visualization. More information: [The Sensoterra Index - Introduction and examples[(https://support.sensoterra.com/hc/en-us/article_attachments/360037916332)

It uses the LoraWan network managed by KPN (NL) or Proximus (BE) to send updates every hour in an asynchronous manner, meaning that each device has its own updating time.

## How does it work
Soil moisture plays an important role in agricultural monitoring, drought and flood forecasting, forest fire prediction, water supply management, and other natural resource activities. Soil moisture observations can forewarn of impending drought or flood conditions before other more standard indicators are triggered.

As defined by the [AMS Glossary of Meteorology](https://glossary.ametsoc.org/wiki/Soil_moisture), soil moisture is “the total amount of water, including the water vapor, in an unsaturated soil.” Soil moisture—sometimes also called soil water—represents the water in land surfaces that is not in rivers, lakes, or groundwater, but instead resides in the pores of the soil. The level of soil moisture is determined by a host of factors beyond weather conditions, including soil type and associated vegetation. In turn, soil moisture levels affect a range of soil and plant dynamics. Surface soil moisture is the water that is in the upper 10 cm of soil, whereas root zone soil moisture is the water that is available to plants—generally considered to be in the upper 200 cm of soil.

## Manufacturer

<!-- ![]({static}/images/devices/sensoterra_screen2.png) -->

On the [Sensoterra's website](https://www.sensoterra.com/) we can read

> We develop wireless soil moisture devices, providing actionable insights that enable water management platforms and solutions.

it is mainly used by:

* agriculture and irrigation control, **agriculture optimisation**
* climate-resilient cities, **smart cities**
* water & environmental monitoring

## Setup

![]({static}/images/devices/sensoterra_1.jpg "Spraying the device in dark brown to camouflage it")

The Sensoterra devices are relatively easy to set up.
They carry a QR-code that has to be scanned through the Sensoterra smartphone app.
One has to configure the soil type to the specific area, for example, the ones in the Sonian Forest Brussels are set to Silt Loam 2 (4.7% org.) following this tool by [Leefmilieu Brussel](https://geodata.environnement.brussels/client/brugeotool/technical/152991/165041.)
The sensors in Arnhem are configured to different types of soil based on [https://bodemdata.nl/basiskaarten](https://bodemdata.nl/basiskaarten). The underground changes from one place to another, due to the history of the landscape - [Push moraine landscape](https://en.wikipedia.org/wiki/Push_moraine):
- Douglas fir: Sand 6 (4.2% org.)
- Solitary Oak: Sand 6 (4.2% org.)
- Forest Oak: Sand 1.1 (0.0% org)
- Red Beech: Sand 6 (4.2% org)
- Horse chestnut: Sand 6 (4.2% org)
- Sweet chestnut: Sand 1.1 (0.0% org)

Next, we shake the device to activate it and put it inside the soil with a rubber hammer.
They must be placed near the dripping line of the tree.

The activation can be a little tricky, since it doesn't have any light indicator for whether it's on or off.
We also discovered that they have a deep sleep mode. This is a functionality to preserve battery use. It turns them off when they are being shaken, for example during car transport, which was our case.

<!-- ![]({static}/images/devices/sensoterra_3.jpg "The device hiding behind a leaf") -->

[full installation guide](https://www.sensoterra.com/wp-content/uploads/2023/05/SD-Installation-guide-v1.3.pdf)

## Data

![]({static}/images/devices/sensoterra_screen.png)

Sensoterra provides a cloud platform on which we can see all the Sensoterra devices we connected to our account.
From there, we can export the data in various spreadsheet formats, and get a geolocalisation of all the devices. The devices don't have an integrated GPS, and their positions are added by scanning them with a smartphone in the setup process.

They have a REST API from which it is relatively easy to request a list of all devices on our account, and then request the data from each devices, through python request module, using an authentification token.

Each devices send new data only **every hour**, one for each of the available units.

* Soil temperature in `°C`.
* Soil moisture in `%`.
* Soil moisture in the sensoterra index (`SI`), a pre-computed form of the soil moisture according to a pre-configured soil types per devices.

Each devices update **asynchronously**, meaning that we get update on for each tree at different moment during the 1 hour time frame.

For this project we're using soil temperature in `°C` and Soil moisture in `%`.

## Why measuring soil moisture and soil temperature?

Water (and minerals) are transported from the roots to the canopy through the xylem, a cell wall behind the bark, that is formed as the plant expands in girth and builds a ring of new xylem around the original primary xylem tissues. When this happens, the primary xylem cells die and lose their conducting function, forming a hard skeleton that serves only to support the plant (a tree ring). The word xylem is derived from the Ancient Greek word ξύλον (xylon), meaning "wood"

"Water is indispensable in tree growth because all biochemical processes in the tree take place in an aqueous environment. Absorption of carbon dioxide and water release through the stomata are linked, so growth at a given site is proportional to transpiration. Transport of water takes place through the wood tissue (xylem) in the trunk, with the driving force being suction tension in the leaves, created by water loss on evaporation. Drought can be caused by moisture deficiency in the soil, but also by dry air combined with high temperature and high solar radiation.
Trees can adapt somewhat to drought by dropping leaves or needles, reducing the evaporating surface, and the tree
is less at risk of dying off. This phenomenon is well observable in dry years, for example in Scots pine and birch.

Terrestrial plants grow in a relatively dry environment, and during evolution have developed all kinds of adaptations to minimise water loss to the environment. These adaptations include a low permeability leaf surface (the cuticle) containing stomata that can be opened and closed depending on moisture availability to regulate water loss. In general, the stomata are closed in the dark, and open when it gets light. In the field, this means that the exchange of water and carbon dioxide takes place mainly during the day, with the temperature of the air and the amount of solar radiation as major determining factors. Most of the energy required for evaporation is derived from incoming radiation, while the drying power of the air is largely determined by air moisture and temperature. At higher temperatures, relative humidity drops, and evaporation demand is greater. All in all, this results in greater transpiration during days with lots of sunshine and high temperatures.
These transpiration losses are compensated by root uptake of water from the soil. If the water loss is too great and the subsequent supply from the soil falls short, the plant dries out and dies. Of the trunk wood, only the sapwood is taken into account because the heartwood is not physiologically active, and does not participate in the water balance of the tree. In trees, about 50% of the fresh weight of the living biomass consists of water; heartwood generally contains 25-30% water. With a total living biomass of about 200 tonnes per hectare, this means about 100 tonnes of water (100 m3 ha-l).
Not all this water is freely exchangeable, as it is partly bound in cell walls etc. The amount of water that is directly exchangeable, without major physiological changes, is in the order of 10%, i.e. about 10 tonnes per hectare, or 1 kg per m2.
This corresponds to a water column of 1 mm. If we consider that transpiration in summer is on the order of 2-4 mm of water per day, it becomes clear that the biomass is far from containing enough water to meet the daily demand for evaporation.""
Source: [Waterrelaties bij bomen](https://edepot.wur.nl/114862)

Read more about water regulation in old growth Douglas firs:
[Tree water storage and its diurnal dynamics related to sap flow and changes in stem volume in old-growth Douglas-fir trees by JAN CERMAK, JIRI KUCERA, ea, 2007](https://www.fs.usda.gov/pnw/pubs/journals/pnw_2007_cermak001.pdf)
