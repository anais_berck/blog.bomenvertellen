title: CO2-devices


## Device

![]({static}/images/devices/milesight.jpg)

CO₂ is a colorless, odorless gas that’s soluble in water and often seen as the bubbles in fizzy drinks. But it’s also a greenhouse gas, a by-product that’s released when we burn materials containing carbon, as well as a gas formed in the respiratory and metabolic processes of living organisms, which can also be considered slow burning reactions. As CO₂ is always a result of a burning reaction, it is non-combustible and inert. At temperatures below -79 °C CO₂ becomes solid, and then it’s known as dry ice, which is typically used in transportation or cargo of frozen goods. A

During COVID-19 sensors started to become fashionable to keep the CO₂ concentration at the recommended level of below 1000 ppm inside places like office buildings, hospitals, and schools; and to start opening windows when the level was too high.
In greenhouses the CO₂ concentration is kept at a higher level than the ambient concentration of about 400 ppm CO₂. There, CO₂ is used as a fertilizer for the plants. In growth or storage chambers for biological samples, CO₂ levels are kept elevated, often at 5 %CO₂, which together with elevated temperature and high humidity ensure optimal growth conditions for the specimens.


## Manufacturer

For CO2, we ended up with trying three manufacturers.

* Seeeeds China and SenseCap
* Milesight
* Decentlab < bought by Bosch, SenseAir, Sensirion

what is the branding of each manufacturers?

### Seeeeds China and SenseCap

![]({static}/images/devices/seeeds_branding.png)

The story about sensecap is interesting.

[Seeeds](https://www.seeedstudio.com/) is a tech chinese company.
Even though they work within **Smart Agriculture**, the technology they sell is not especially related to aggronomy (their name is a simple example of the conforting feeling of using plant words when talking about technology as an obvious marketing strategy).
Their navigation menu offers three scenarios under the solution tab:

* Environment Monitoring Solutions
* Intelligent Video Analytics Solutions
* Positioning Solutions

Under the product tab, amongst other, we can find SenseCAP product: LoRaWAN devices for Smart Agriculture (measuring different weather parameter, CO2, ...), but not only! There is also the **SenseCAP M4 Square-FluxNode**, that doesn't measure anything.


With a bit more research on [SenseCap](https://www.sensecapmx.com/) as a separate entity than Seeeds (it looks like Seeeds might just be a re-seller), their branding feel different. No more plants icons and soft grey tones, but dark theme and slick neon tech borders.
Now they talk about **Helium** and the **HNT cryptocurrency**.

![]({static}/images/devices/sensecap_branding.png)

In fact Helium is a decentralized network that uses the LoRaWAN protocol, where people can buy nodes (such as the SenseCAP M4 Square-FluxNode). The idea is that by adding a node to the network (setting it up in your living room or garden), you spread the Helium network a little bit, and you get paid in the HNT cryptocurrency for doing so.
The whole thing was a scam: see article.

There are multiple possibilities: did SenseCap develop CO2 and Smart Agriculture sensors only because they already have the infrastructure and production line to work with LoRaWAN technologies (to enlarge their gamme of product), or did they started with smart aggriculture and realised they could make money by selling Helium node afterward.

The paradoxical connection there is a bit creepy: between something that measures CO2, and the production of cryptocurrency as something that cost CO2 (to construct the nodes hardware and develop the Helium infrastructure).
Corporation making money by selling devices that can measure their own environmental impact, disguissed as "smart" and soft grey/green aggriculture.

### Milesight

for Smarthouses?

### Decentlab

for scientific researches?

## Setup

Sensecap, Milesight and Decentlab where mounted on a branch of the tree.
We had to put them in at a certain high to prevent people to interact with those.
We got help from a team of climbers and a (the machine with the platform).

!!pictures of setting up the sensors!!

The Milesight ones asked for a small preparation: unscrewing and connecting the battery on the inside, then pressing an initialisation button (only accessible from the inside of the device), then screwing it back again.

Other than that, those sensors doen't need to be in precise relationship with the tree, as they are measuring something more ambient around it.

## Data

<!-- devices 2 api 2 app pipeline, what did they provide? -->

### Sensecap

They have a REST API from which it is relatively easy to request a list of all devices on our account, and then request the data from each devices, through python request module, using an authentification key and id.

However, The sensecap API changed during the setting up of our code.
This felt like something you have to deal it when working with high-tech corporation/startup: keeping up with the speed everything update become a challenge you have to implement.

Each devices register new data every **5 minutes**, one for each of the available units.

* Air temperature in `°C`.
* Air humidity in `%RH`.
* CO2 in `ppm`

### Milesight

### Decentlab


## Why measuring CO2?
CO2 is alimentation for trees. During the day they absorb CO2 throrugh the stomata of the leaves and use it - in combination with sunlight and water - for photosynthesis and the creation of sugars, and producing oxygen as waste. The carbon is stocked in the wood cells of the tree. When a tree is cut and left to decay or when it is burnt, the carbon hooks up again with the oxygen and CO2 is released back into the atmosphere.
At night only about half that carbon is then released through respiration.

Land plants remove about 29 per cent of all our CO2 emissions from the air, emissions that would otherwise contribute to the increase in atmospheric CO2 levels.
But they release the CO2 back in the air when they are cut and burnt or left behind to organic decay. The CO2 is kept in the wood when it is used for construction.
Although some CO2 released by respiring cells in tree stems diffuses directly to the atmosphere, on a daily basis 15–55% can remain within the tree.

It may be possible to plant and conserve enough forests to remove from the atmosphere an amount of carbon equivalent to a cut in business-as-usual fossil fuel emissions of 12–15 per cent between now and 2050. But this would not stabilize atmospheric CO2 levels nor hold them at below 600 p.p.m. next century, which some regard as necessary to avoid ‘dangerous’ climatic change. Forestry is, therefore, a contributor to the solution, not the sole answer; there
is no avoiding having to cut fossil fuel emissions.
At the UK scale, the annual exchange of carbon between the land and atmosphere is of the order 100–150 MtC a–1 (Metric tons of carbon). This figure is based on predictions of gross photosynthesis made using the Hurley Pasture and Edinburgh Forest models at sites in lowland England and upland Scotland (Thornley, 1998 and personal communication). Note that this ‘natural’ carbon exchange is similar to the amount of carbon added into the atmosphere each year by burning fossil fuels [Common questions about CO2](https://watermark.silverchair.com/720237.pdf)

Plants & trees use 'evo-vaporation' while absorbing CO2 form the atmosphere: their leaves, for 100% filled with water, have stomata (little mouths) that open up to absorb CO2. While opening up, they release water. CO2 and sunlight is needed to transform into sugars, cellulose in the plants leaf cells. Oxygen is released as waste.
"The sugars are transported down to the roots through the phloem, the innermost layer of the bark, hence the name, derived from the Ancient Greek word φλοιός (phloiós), meaning "bark".
Unlike xylem (which is composed primarily of dead cells), the phloem is composed of still-living cells that transport sap. The sap is a water-based solution, but rich in sugars made by photosynthesis. These sugars are transported to non-photosynthetic parts of the plant, such as the roots, or into storage structures, such as tubers or bulbs.
During the plant's growth period, usually during the spring, storage organs such as the roots are sugar sources, and the plant's many growing areas are sugar sinks. The movement in phloem is multidirectional, whereas, in xylem cells, it is unidirectional (upward).""
Source: [Wikipedia](https://en.wikipedia.org/wiki/Phloem)

The transport in the phloem can be compared to small roads traversing villages, while that of the xylem is like a highway.

"Scientists agree that CO2 acts as a kind of fertiliser for plant growth.
"There is evidence that trees show more leaf growth and produce more wood," says Cernusak, ecophysiologue at teh Australian James Cook University. "The wood is where most of the carbon is stored in the plant mass."
Scientists at Oak Ride National Laboratory observed that in plants exposed to elevated levels of CO2, stomata in their leaves became larger.
Plants exposed to twice the amount of CO2 they were used to, turn out to change a bit the anatomy of their leaf tissues. That makes it harder for herbivores to eat them and harder for larvae to grow on them.
Scientists are afraid that plants will reach their maximum CO2 absorption soon. This might affect climate change even more."
Source: [https://www.nationalgeographic.nl/natuur-leefomgeving/2019/05/planten-helpen-onze-co2-te-absorberen-maar-hoelang-nog](https://www.nationalgeographic.nl/natuur-leefomgeving/2019/05/planten-helpen-onze-co2-te-absorberen-maar-hoelang-nog)"

Currently, around 25 per cent of carbon emissions from the use of fossil fuels is being taken up and stored by plants. This positive contribution of plants may decline in the future as they begin to respire more as the world warms. (https://www.nature.com/articles/s41467-017-01774-z)
