title: Band dendrometers

## Measurements

![]({static}/images/devices/esmbrno.jpg)

Trunk dendrometers are a device that is attached to the side of a tree and measure the minuscule changes in the diameter of a tree's trunk as water availability, tree condition, weather, and other influences change throughout a day, night, and season.
Trees are continually changing shape. During the day, as water is drawn up through the outer layer of the tree to the leaves, the trunk contracts. At night, as it fills with water, it expands. As the sun comes up and the water starts moving through the tree, the sides will come in just a little bit. A few microns. You will never see this with your eyes. But you can measure it.
Trees grow mostly at night, from a few weeks before the leafs appear till more or less Mid-July. In Spring, every night, the wood and bark of the cambium—the cellular tissue between a tree’s bark and wood—enlarges until the sun comes up. Then, during the day, as the temperature increases, the tree goes through small micron-level contractions—shrinkage caused by tension in its molecules as they pull water up from the roots and deliver it to the leaves. In the sunlight, the pores of the leaves open, letting the water out and the carbon dioxide in—a whole tree, from roots to branches, breathing.

## Manufacturer

![]({static}/images/devices/EMS_branding.png)

We bought the band dendrometers at the Czech company ESMBrno. The abbreviation stands for 'Environmental Measuring Systems s.r.o.'. They deal with 'Data Acquisition Environment Hardware – Software – Cloud application'. As we were looking for non-invasive dendrometers, we came across the band dendrometers (opposed to point dendrometers). EMSBrno develops their own sensors and sells them around the world to companies who rebrand the products.

## Uses

Various insights into tree growth and physiology can be obtained using high-resolution dendrometers, including the assessment of stem daily water status and the understanding of short-term growth responses to changing environmental conditions.
From the paper [Tree allometry of Douglas fir and Norway spruce on a nutrient-poor and a nutrient-rich site](http://www.emsbrno.cz/r.axd/pdf_v_Urban__2013_u_pdf.jpg)
Allometry has a long history in plant studies, as scientists searched for interdependence among tree morphological characteristics. Allometry, in its broader sense, as will be considered in this study, is concerned with the size of organisms and its consequences for their shape and function
(Niklas 1994; Pretzsch 2010). For this reason we decided to use tree allometry to describe differences among the tree species from two climatically similar sites which differed in soil nutrient availability. Tree biometric relationships vary considerably not only with species and geographic location but also with stand age, site quality, climate and stocking density of stands (Cannell 1982; Bartelink 1997; Poorter et al. 2012). In theory, allometric equations describing tree dimensions are affected by the physiological requirements of the tree; that is, form and function are related. The most important of these requirements are water transport, light interception, and mechanical support against gravity or wind (Niklas 1994). Consequently, allometry is an effective tool for evaluating differential plant responses to site conditions (Pretzsch 2010; Poorter et al. 2012)
From [The New Yorker, August 2019](https://www.newyorker.com/science/elements/a-day-in-the-life-of-a-tree) an article about Jeremy Hise and Kevin Griffin, a professor of earth and environmental sciences at Columbia University, 'planning to build the largest network of dendrometers in the world'. They sell affordable precision dendrometers to citizens who can then follow the mouvements of the sensor on the cloud-based platform called the EcoSensor Network:
'That April, [Griffin[(https://ecosensornetwork.com/)] started to see radical changes in the oak trees’ data, indicating that growth had started. But, confusingly, the trees had no leaves. “How in the world have they started growing two weeks prior to leaf out?” he wondered. It turned out that cell division and radial stem growth started two weeks before leaf development—something that Griffin hadn’t known even “after twenty years of being a professor and working with trees.” Similarly, Griffin predicted that the trees would continue to grow until their leaves began to fall, in autumn. Instead, growth came to a halt in mid-July. He now believes that trees suspend growth for the second half of the summer in order to store carbon, which they use to grow new wood before the leaves come out the following spring.'

## Setup

![]({static}/images/devices/EMS_0.jpg "Showing the team how to setup the dendrometer at ground level, so they can do it in the air.")

![]({static}/images/devices/EMS_1.jpg)

![]({static}/images/devices/EMS_2.jpg)

Tree workers of Gemeente Arnhem helped to install the dendrometers by using an arial work platform near the Horse chestnut and the Sweet chestnut. Tree climbers installed the dendrometers at the Solitary Oak, the Forest Oak and the Red beech. They all closely watched this [installation video](http://www.emsbrno.cz/p.axd/en/EMScloud.Increment.02.html).

## Data

The dendrometers use the [Sigfox](https://en.wikipedia.org/wiki/Sigfox) network, another Low Power Wide Area Network (LPWan) like LoraWan. This network is deprecated in Czech Republic, but active and covering the Netherlands.

![]({static}/images/devices/EMS_data.png)

![]({static}/images/devices/EMS_data_2.png)

In Belgium the network is managed by [Citymesh](https://www.citymesh.com/).

In the Netherlands the network is managed by [Hyrde]( https://hyrde.io/).

They manually setup an entry for us in their [list of organisation](http://www.emsbrno.cz/p.axd/en/Online.Data.html) (as Anais Berk). It's curious to see that all those data are open.

The interface of their online service feels a lot more raw than the IoT "Smart" solutions.
The datas are displayed in a more academic and scientific aesthetic: default fonts, old school greys and blue links, with table and rough graphs generated in PHP. 

The dendrometer sends data to Hyrde, who forwards it to the EMScloud. 
The database sends us new data **every 10 mins**, on an endpoint we have to configure on our side.
Which is a bit more techy and complex operation that easy curl request to a REST API, and we have to communicate them our endpoint by email for them to be added manually. A not really scalable solution which feels re-assuring in a way.

The URL from the call can be used to download the new data (there is more URLs in the call - for datafiles containing last 2, 5, 10, 30 and 60 days). Each of these URLs can be used 3 times and is valid for 2 hours.


