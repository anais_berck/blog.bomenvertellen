# blog.bomenvertellen

A blog where we document the project _bomen vertellen_ aka _gesitueerd portret_

install

    pip install -r requirements.txt

generate

    pelican -lr

then go to <http://localhost:8000>
